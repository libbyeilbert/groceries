from django.urls import path
from inventory.views import shopping_list, single_source, list_all

urlpatterns = [
    path("home/", list_all, name="home"),
    path("shop/<slug:slug>/", single_source, name="source"),
    path("shop/", shopping_list, name="shopping_list"),
]
