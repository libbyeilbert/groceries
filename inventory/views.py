from django.shortcuts import render, get_object_or_404
from inventory.models import Item, Source
from recipes.models import Ingredient


def list_all(request):
    items = Item.objects.all()
    ingredients = Ingredient.objects.all()
    context = {
        "items": items,
        "ingredients": ingredients,
    }
    return render(request, "inventory/list.html", context)


def shopping_list(request):
    items = Item.objects.filter(in_stock=False)
    ingredients = Ingredient.objects.filter(in_stock=False)
    context = {
        "items": items,
        "ingredients": ingredients,
    }
    return render(request, "inventory/shopping_list.html", context)


def single_source(request, slug):
    source = get_object_or_404(Source, slug=slug)
    context = {
        "source": source,
    }
    return render(request, "inventory/source.html", context)
