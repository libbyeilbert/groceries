from django.db import models
from django.urls import reverse
from django.template.defaultfilters import slugify


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Source(models.Model):
    name = models.CharField(max_length=50)
    slug = models.SlugField(
        null=True,
        unique=True,
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("source", kwargs={"slug": self.slug})

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        return super().save(*args, **kwargs)


class Item(models.Model):
    name = models.CharField(max_length=50)
    category = models.ForeignKey(
        Category,
        related_name="items",
        on_delete=models.PROTECT,
    )
    in_stock = models.BooleanField(default=True)
    source = models.ForeignKey(
        Source,
        related_name="items",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.name
