from django.urls import path
from recipes.views import list_recipes

urlpatterns = [
    path("", list_recipes, name="recipes"),
]
