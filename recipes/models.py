from django.db import models
from inventory.models import Source


class Recipe(models.Model):
    title = models.CharField(max_length=50)
    CATEGORY_CHOICES = (
        ("breakfast", "breakfast"),
        ("dinner", "dinner"),
        ("premade", "premade"),
        ("special", "special"),
        ("drinks", "drinks"),
    )
    category = models.CharField(
        max_length=10,
        choices=CATEGORY_CHOICES,
    )
    ingredients = models.ManyToManyField('Ingredient', blank=True)

    def __str__(self):
        return (self.title)


class Ingredient(models.Model):
    name = models.CharField(max_length=50)
    source = models.ForeignKey(
        Source,
        related_name="ingredients",
        on_delete=models.PROTECT,
        blank=True,
        null=True,
    )
    recipes = models.ManyToManyField(Recipe, blank=True)
    in_stock = models.BooleanField(default=True)

    def __str__(self):
        return self.name
