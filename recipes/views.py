from django.shortcuts import render
from recipes.models import Recipe

def list_recipes(request):
    recipes = Recipe.objects.all()
    context = {
        "recipes": recipes,
    }
    return render(request, "recipes/list.html", context)
